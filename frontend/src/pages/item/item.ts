import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { Shop } from '../../domain/store/shop';
import { Item } from '../../domain/store/item';
import { CartPage } from '../cart/cart';
import { ItemService } from '../../providers/item-service';

/*
  Generated class for the Item page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-item',
  templateUrl: 'item.html'
})
export class ItemPage {
  public item: Item;
  

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController,
    private _alertCtrl: AlertController,
    public itemService: ItemService) {
    
  }

  ngOnInit() {
    this.item = this.navParams.get('itemSelect');
  }

  addCart(item){
    //this.navCtrl.push(CartPage,{item:Item});
    this.presentToast("Item adicionado ao carrinho");
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
